#!/bin/bash

clear

printf "New alias : \t"
read alias

printf "Command : \t"
read cmd

echo "alias $alias='$cmd'" >> ~/.bash_aliases
printf "\nDone :)\n"

bash
