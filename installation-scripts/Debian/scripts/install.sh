#!bin/bash

# # This file is just designed to try scripts on a VM
# To do so, please create a	 shared folder between the local machine and the VM (the correct extensions should be installed)
# The shared folder should be named "shared"
# It will create a folder on the VM 'sharedFolder' that is a copy of the local folder.
# From this point it is able to copy the scripts files and run the setup.sh file
# You have to run the others scripts manually (optionnal) 

sudo mount -t vboxsf shared sharedFolder

rm setup.sh || true
sudo cp sharedFolder/setup.sh .
sudo chown test:test setup.sh
chmod +x setup.sh

rm setup2.sh || true
sudo cp sharedFolder/setup2.sh .
sudo chown test:test setup2.sh
chmod +x setup2.sh

./setup.sh


