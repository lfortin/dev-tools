#!/bin/bash

echo " "
echo " "
echo " "
echo "***************************************"
echo "         Installation de Node          "
echo "***************************************"
nvm install node
nvm install stable
echo 'O' | sudo npm install --global nodemon


echo " "
echo " "
echo " "
echo "***************************************"
echo "                 Clear                 "
echo "***************************************"
echo 'O' | sudo apt autoremove


echo " "
echo " "
echo " "
echo "Finit φ(*⌒▽⌒)ﾉ"

exec bash
