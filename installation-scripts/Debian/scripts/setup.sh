#!/bin/bash

# Variables
NVM_VERSION=0.40.1

VSCODE_SETTINGS_URL=https://gitlab.com/lfortin/dev-tools/-/raw/master/installation-scripts/Ubuntu/vscode.settings.json
VSCODE_KEY_BINDING_URL=https://gitlab.com/lfortin/dev-tools/-/raw/master/installation-scripts/Ubuntu/vscode.keybindings.json

PROFILE_PICTURE_URL=https://gitlab.com/lfortin/dev-tools/-/raw/master/installation-scripts/Ubuntu/.face
CINNAMON_SETTINGS_URL=https://gitlab.com/lfortin/dev-tools/-/raw/master/installation-scripts/Ubuntu/cinnamon_backup

SHELL_NAME=zsh

echo " "
echo " "
echo "***************************************"
echo "                                       "
echo "       Ça part en installation !       "
echo "                                       "
echo "***************************************"
echo " "
echo " "

sudo apt update
sudo apt upgrade -y
sudo apt dist-upgrade -y

sudo apt install -y ca-certificates curl gnupg lsb-release git make gcc apt-transport-https aptitude

sudo aptitude -y safe-upgrade

echo " "
echo " "
echo " "
echo "***************************************"
echo "          Installation de vim          "
echo "***************************************"
sudo -S apt install -y vim
git config --global core.editor "vim"

echo " "
echo " "
echo " "
echo "***************************************"
echo "          Installation de NVM          "
echo "***************************************"
sudo curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v${NVM_VERSION}/install.sh | bash

echo " "
echo " "
echo " "
echo "***************************************"
echo "          Installation de NPM          "
echo "***************************************"
yes | sudo apt install npm
yes | sudo npm install --global yarn

echo " "
echo " "
echo " "
echo "***************************************"
echo "               Dev tools               "
echo "***************************************"
yes | sudo apt install build-essential manpages-devO
yes | sudo apt install gobjc gfortran gnat
yes | sudo apt install zsh
echo 'Y' | sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"


echo " "
echo " "
echo " "
echo "***************************************"
echo "       Installation de VSCodium        "
echo "***************************************"
snap install codium --classic

# get VSCodium settings and keybindings
# initialize vscode settings first
codium
wget -O settings.json ${VSCODE_SETTINGS_URL}
wget -O keybindings.json ${VSCODE_KEY_BINDING_URL}
while [ ! -d ~/.config/VSCodium ] ; 
# vscode is not initialized yet
do
  echo "VS Codium not initialized" 
  sleep 0.5
done

if [ -f ./settings.json ]; then
	mv settings.json ~/.config/VSCodium/User/settings.json
fi
# close vscodium, settings could now be copied
sudo pkill -9 codium 
# copy files
if [ -f ./settings.json ]; then
	mv settings.json ~/.config/VSCodium/User/settings.json
fi
if [ -f ./keybindings.json ]; then
	mv keybindings.json ~/.config/VSCodium/User/keybindings.json
fi

# install extensions
codium --install-extension vivaxy.vscode-conventional-commits
codium --install-extension ms-azuretools.vscode-docker
codium --install-extension p1c2u.docker-compose
codium --install-extension dbaeumer.vscode-eslint
codium --install-extension MS-CEINTL.vscode-language-pack-fr
codium --install-extension waderyan.gitblame
codium --install-extension mhutchie.git-graph
codium --install-extension donjayamanne.githistory
codium --install-extension eamodio.gitlens
codium --install-extension esbenp.prettier-vscode
codium --install-extension Codeium.codeium

echo " "
echo " "
echo " "
echo "***************************************"
echo "                Scripts                "
echo "***************************************"
wget https://gitlab.com/lfortin/dev-tools/-/raw/master/addAlias.sh
wget https://gitlab.com/lfortin/dev-tools/-/raw/master/exec.sh
wget https://gitlab.com/lfortin/dev-tools/-/raw/master/test.sh
wget -O aliases https://gitlab.com/lfortin/dev-tools/-/raw/master/.aliases
wget -O rc https://gitlab.com/lfortin/dev-tools/-/raw/master/.bashrc

mkdir -p ~/scripts
mv *.sh ~/scripts
chmod +x ~/scripts/*.sh
mv aliases ~/.aliases
cat rc >> ~/.${SHELL_NAME}rc
rm rc


echo " "
echo " "
echo " "
echo "***************************************"
echo "                Docker                 "
echo "***************************************"
# Set up the repository
sudo apt-get update
sudo apt install apt-transport-https ca-certificates curl gnupg-agent software-properties-common

# Add Docker’s official GPG key:
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"

# Install Docker Engine
sudo apt remove docker docker-compose docker-doc docker-registry docker.io
sudo apt-get install docker-ce docker-ce-cli containerd.io

sudo usermod -a -G docker $USER
sudo groupadd docker && sudo gpasswd -a $USER docker && sudo systemctl restart docker


# echo " "
# echo " "
# echo " "
# echo "***************************************"
# echo "               Browsers                "
# echo "***************************************"
yes | sudo apt install chromium-browser --fix-missing


echo " "
echo " "
echo " "
echo "***************************************"
echo "                Others                 "
echo "***************************************"
# Gimp
yes | sudo apt install gimp
# Tutanota
mkdir -p ~/AppImages
wget https://mail.tutanota.com/desktop/tutanota-desktop-linux.AppImage -O ~/AppImages/tutanota-desktop-linux.AppImage
sudo chmod 500 ~/AppImages/tutanota-desktop-linux.AppImage
# Spotify
yes | curl -sS https://download.spotify.com/debian/pubkey_5E3C45D7B312C643.gpg | sudo apt-key add - 
yes | echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list
sudo apt-get -y update && sudo apt-get -y install spotify-client


echo " "
echo " "
echo " "
echo "***************************************"
echo "                Layout                 "
echo "***************************************"
wget -O ~/.face ${PROFILE_PICTURE_URL}
wget -O ~/cinnamon_backup  ${CINNAMON_SETTINGS_URL}
dconf load /org/cinnamon/ < ~/cinnamon_backup

# shortcuts
# switch to workspace
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-left "['<Control><Alt>Left']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-right "['<Control><Alt>Right']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-up "['<Control><Alt>Up']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-down "['<Control><Alt>Down']"
#  move window to workspace
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-left "['<Control><Alt><Shift>Left']"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-right "['<Control><Alt><Shift>Right']"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-up "['<Control><Alt><Shift>Up']"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-down "['<Control><Alt><Shift>Down']"
# maximize/minimize
gsettings set org.gnome.desktop.wm.keybindings maximize-vertically "['<Super>Up']"
gsettings set org.gnome.desktop.wm.keybindings minimize "['<Super>Down']"
# only switch main screen from workspaces
gsettings set org.gnome.mutter workspaces-only-on-primary true
# disable trackpad when mouse is plugged
gsettings set org.gnome.desktop.peripherals.touchpad send-events disabled-on-external-mouse

echo " "
echo " "
echo " "
echo "***************************************"
echo "                 Clear                 "
echo "***************************************"
yes | sudo apt autoremove


echo " "
echo " "
echo " "
chsh -s $(which ${SHELL_NAME})
echo "Finit φ(*⌒▽⌒)ﾉ"
echo "- Please run ./setup2.sh for next installations"
echo "- Please logout or shut computer down to finish docker and zsh configuration"

exec bash
