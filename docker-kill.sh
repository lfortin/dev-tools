#!/bin/bash

if [ $# -eq 1 ]
then
	c=$1
else
	printf "container : \t"
	read ctn
	c=$ctn
fi

docker stop $c
docker rm $c
