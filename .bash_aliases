#sh
alias llc='clear && ls -l'
alias llac='clear && ls -al'
alias exe='~/scripts/exec.sh'
alias _exec='~/scripts/exec.sh'
alias port='sudo lsof -nP -iTCP -sTCP:LISTEN | grep'
alias ports='ps -ef | grep docker-proxy'
#vpn
alias vpnco='nordvpn connect'
alias vpnreco='nordvpn disconect && nordvpnconnect'
alias vpndeco='nordvpn disconnect'
#docker
alias docker-clean='docker image prune && docker volume prune'
alias dockerps='docker ps --format=$DOCKER_FORMAT'
alias dc='docker-compose'
alias dkill='exe docker-kill'